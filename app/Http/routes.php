<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'api'], function () {
    Route::get('employees', 'EmployeesController@index');
    Route::post('employees', 'EmployeesController@store');
    Route::get('employees/{employees}', 'EmployeesController@show');
    Route::match(['put', 'patch'], 'employees/{employees}', 'EmployeesController@update');
    Route::delete('employees/{employees}', 'EmployeesController@destroy');
    // Route::resource('employees', 'EmployeesController');
});

